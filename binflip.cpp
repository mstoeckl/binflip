#include <stdio.h>
#include <unistd.h>
ssize_t read(int fd, void *buf, size_t count);

int main(int argc, char** argv) {
    if (argc != 1) {
        fprintf(stderr, "Usage: binflip < input > output\n");
        return 1;
    }
    
    fprintf(stderr, "Starting...\n");
    char buf[4096];
    ssize_t in;
    ssize_t out;
    while ((in = read(STDIN_FILENO, buf, 4096)) > 0) {
        for (ssize_t i=0;i<in;i++) {
            buf[i] = 0xFF - buf[i];
        }
        out = write(STDOUT_FILENO, buf, in); 
        if (out != in) {
            fprintf(stderr, "Durn got interrupted\n");
            return 1;
        }
    }
    fprintf(stderr, "Success...\n");
    return 0;
}